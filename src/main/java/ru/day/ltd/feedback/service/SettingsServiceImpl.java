package ru.day.ltd.feedback.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.day.ltd.feedback.repo.SettingsRepository;

@Service
public class SettingsServiceImpl implements SettingsService {

    private final SettingsRepository repository;

    @Autowired
    public SettingsServiceImpl(SettingsRepository repository) {
        this.repository = repository;
    }


    @Override
    public String getStringValue(String key) {
        return repository.findByKey(key).getValue();
    }

    @Override
    public Integer getIntegerValue(String key) {
        return Integer.parseInt(repository.findByKey(key).getValue());
    }

    @Override
    public Double getDoubleValue(String key) {
        return Double.parseDouble(repository.findByKey(key).getValue());
    }

    @Override
    public Boolean getBooleanValue(String key) {
        return Boolean.parseBoolean(repository.findByKey(key).getValue());
    }

    @Override
    public Long getLongValue(String key) {
        return Long.parseLong(repository.findByKey(key).getValue());
    }

}
