package ru.day.ltd.feedback.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.day.ltd.feedback.entity.PendingReview;
import ru.day.ltd.feedback.repo.PendingReviewRepository;
import ru.day.ltd.feedback.repo.ReviewRepository;

import javax.persistence.EntityGraph;
import java.util.Date;
import java.util.UUID;

@Service
public class PendingReviewServiceImpl implements PendingReviewService {

    private final FindService findService;
    private final ReviewRepository reviewRepository;
    private final PendingReviewRepository pendingReviewRepository;

    @Autowired
    public PendingReviewServiceImpl(FindService findService, ReviewRepository reviewRepository, PendingReviewRepository pendingReviewRepository) {
        this.findService = findService;
        this.reviewRepository = reviewRepository;
        this.pendingReviewRepository = pendingReviewRepository;
    }

    @Override
    public PendingReview findById(UUID id) {
        return findService.findPendingReviewById(id);
    }

    @Override
    public PendingReview findById(UUID id, EntityGraph<?> graph) {
        return findService.findPendingReviewById(id, graph);
    }

    @Override
    public PendingReview create(UUID userId, UUID bookingEventId) {
        return pendingReviewRepository.save(
            new PendingReview(userId, bookingEventId, new Date())
        );
    }

    @Override
    public Boolean isSatisfied(UUID id) {
        return reviewRepository.existsByPendingReviewId(id);
    }

    @Override
    public Date satisfiedAt(UUID id) {
        return findService.findReviewByPendingReviewId(id, null).getSubmitDate();
    }

    @Override
    public void discard(UUID id) {
        if (isSatisfied(id)) {
            throw new RuntimeException("Нельзя отменить ожидание оценки, т.к. отзыв уже оставлен");
        }
        var pending = findService.findPendingReviewById(id);
        pending.setDiscarded(true);
        pending.setDiscardedAt(new Date());
        pendingReviewRepository.save(pending);
    }

    @Override
    public Date discardedAt(UUID id) {
        return findService.findPendingReviewById(id).getDiscardedAt();
    }

}
