package ru.day.ltd.feedback.service;

import ru.day.ltd.feedback.entity.Rating;
import ru.day.ltd.feedback.entity.Review;

import javax.persistence.EntityGraph;
import java.util.UUID;

public interface ReviewService {

    Review findById(UUID id);

    Review findById(UUID id, EntityGraph<?> graph);

    Review addReview(UUID pendingReviewId, String text, Rating rating);

    void deleteReview(UUID id);

    Review modifyReview(UUID id, String text, Rating rating);

}
