package ru.day.ltd.feedback.service;

public class Properties {

    public static final String minReviewTextLengthKey = "minReviewTextLength";
    public static final Integer minReviewTextLengthValue = 20;
    public static final String maxReviewTextLengthKey = "maxReviewTextLength";
    public static final Integer maxReviewTextLengthValue = 1000;

}
