package ru.day.ltd.feedback.service;

public interface SettingsService {

     String getStringValue(String key);

     Integer getIntegerValue(String key);

     Double getDoubleValue(String key);

     Boolean getBooleanValue(String key);

     Long getLongValue(String key);

}
