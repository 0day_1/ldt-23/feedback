package ru.day.ltd.feedback.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.day.ltd.feedback.entity.PendingReview;
import ru.day.ltd.feedback.entity.Rating;
import ru.day.ltd.feedback.entity.Review;
import ru.day.ltd.feedback.repo.ReviewRepository;

import javax.persistence.EntityGraph;
import java.util.Date;
import java.util.UUID;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final FindService findService;
    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewServiceImpl(FindService findService, ReviewRepository reviewRepository) {
        this.findService = findService;
        this.reviewRepository = reviewRepository;
    }

    @Override
    public Review findById(UUID id) {
        return findService.findReviewById(id);
    }

    @Override
    public Review findById(UUID id, EntityGraph<?> graph) {
        return findService.findReviewById(id, graph);
    }

    @Override
    public Review addReview(UUID pendingReviewId, String text, Rating rating) {
        PendingReview pendingReview = findService.findPendingReviewById(pendingReviewId);
        Review review = new Review(
            pendingReview,
            text,
            rating,
            new Date()
        );
        return reviewRepository.save(review);
    }

    @Override
    public void deleteReview(UUID id) {
        reviewRepository.deleteById(id);
    }

    @Override
    public Review modifyReview(UUID id, String text, Rating rating) {
        var review = findService.findReviewById(id);
        review.setReviewText(text);
        review.setRating(rating);
        return reviewRepository.save(review);
    }


}
