package ru.day.ltd.feedback.service;

import ru.day.ltd.feedback.entity.PendingReview;
import ru.day.ltd.feedback.entity.Review;

import javax.persistence.EntityGraph;
import java.util.Date;
import java.util.UUID;

public interface PendingReviewService {

    PendingReview findById(UUID id);

    PendingReview findById(UUID id, EntityGraph<?> graph);

    PendingReview create(UUID userId, UUID bookingEventId);

    Boolean isSatisfied(UUID id);

    Date satisfiedAt(UUID id);

    void discard(UUID id);

    Date discardedAt(UUID id);

}
