package ru.day.ltd.feedback.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.day.ltd.feedback.GraphType;
import ru.day.ltd.feedback.entity.PendingReview;
import ru.day.ltd.feedback.entity.Review;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.UUID;

@Service
public class FindService {

    private final EntityManager em;

    @Autowired
    public FindService(EntityManager entityManager) {
        this.em = entityManager;
    }

    public PendingReview findPendingReviewById(UUID id) {
        return findPendingReviewById(id, null);
    }

    public PendingReview findPendingReviewById(UUID id, EntityGraph<?> graph) {
        var query = em.createQuery(
            "select p from pendingReview p where p.id = :id", PendingReview.class
        )
            .setParameter("id", id);
        if (graph != null) {
            query.setHint(GraphType.LOAD, graph);
        }
        return query.getSingleResult();
    }

    public Review findReviewById(UUID id) {
        return findReviewById(id, null);
    }

    public Review findReviewById(UUID id, EntityGraph<?> graph) {
        var query = em.createQuery(
            "select r from review r where r.id = :id", Review.class
        )
            .setParameter("id", id);
        if (graph != null) {
            query.setHint(GraphType.LOAD, graph);
        }
        return query.getSingleResult();
    }

    public Review findReviewByPendingReviewId(UUID id, EntityGraph<?> graph) {
        var query = em.createQuery(
            "select r from review r where r.pendingReview.id = :id", Review.class
        )
            .setParameter("id", id);
        if (graph != null) {
            query.setHint(GraphType.LOAD, graph);
        }
        return query.getSingleResult();
    }

}
