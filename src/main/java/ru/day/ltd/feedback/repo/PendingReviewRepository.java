package ru.day.ltd.feedback.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.day.ltd.feedback.entity.PendingReview;

import java.util.UUID;

@Repository
public interface PendingReviewRepository extends JpaRepository<PendingReview, UUID> {


}
