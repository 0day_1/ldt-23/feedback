package ru.day.ltd.feedback.entity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "REVIEW")
@Entity(name = "review")
public class Review extends BasicEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pending_review_id", nullable = false)
    protected PendingReview pendingReview;

    @Column(name = "submit_date", nullable = false)
    protected Date submitDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "rating", nullable = false)
    protected Rating rating;

    @Column(name = "review_text", nullable = false, columnDefinition = "TEXT")
    protected String reviewText;

    public Review() {}

    public Review(PendingReview pendingReview, String reviewText, Rating rating, Date submitDate) {
        this.pendingReview = pendingReview;
        this.reviewText = reviewText;
        this.rating = rating;
        this.submitDate = submitDate;
    }

    public PendingReview getPendingReview() {
        return pendingReview;
    }

    public void setPendingReview(PendingReview pendingReview) {
        this.pendingReview = pendingReview;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }
}
