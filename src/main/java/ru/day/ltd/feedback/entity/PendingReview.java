package ru.day.ltd.feedback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Table(name = "PENDING_REVIEW")
@Entity(name = "pendingReview")
public class PendingReview extends BasicEntity {

    @Column(name = "user_id", nullable = false)
    protected UUID userId;

    @Column(name = "booking_event_id", nullable = false, unique = true)
    protected UUID bookingEventId;

    @Column(name = "pending_start_date", nullable = false)
    protected Date pendingStartDate;

    @Column(name = "is_discarded", nullable = false)
    protected Boolean isDiscarded;

    @Column(name = "discarded_at", nullable = false)
    protected Date discardedAt;

    public PendingReview(UUID userId, UUID bookingEventId, Date pendingStartDate) {
        this.userId = userId;
        this.bookingEventId = bookingEventId;
        this.pendingStartDate = pendingStartDate;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getBookingEventId() {
        return bookingEventId;
    }

    public void setBookingEventId(UUID bookingEventId) {
        this.bookingEventId = bookingEventId;
    }

    public Date getPendingStartDate() {
        return pendingStartDate;
    }

    public void setPendingStartDate(Date pendingStartDate) {
        this.pendingStartDate = pendingStartDate;
    }

    public Boolean getDiscarded() {
        return isDiscarded;
    }

    public void setDiscarded(Boolean discarded) {
        isDiscarded = discarded;
    }

    public Date getDiscardedAt() {
        return discardedAt;
    }

    public void setDiscardedAt(Date discardedAt) {
        this.discardedAt = discardedAt;
    }
}
