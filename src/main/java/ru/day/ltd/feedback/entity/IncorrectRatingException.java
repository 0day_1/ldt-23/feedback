package ru.day.ltd.feedback.entity;

public class IncorrectRatingException extends RuntimeException {

    public IncorrectRatingException(Double value) {
        super(String.format("Неправильное значение рейтинга типа Double: %s", value.toString()));
    }

    public IncorrectRatingException(Integer value) {
        super(String.format("Неправильное значение рейтинга типа Integer: %s", value.toString()));
    }
}
