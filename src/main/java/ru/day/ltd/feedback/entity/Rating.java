package ru.day.ltd.feedback.entity;

public enum Rating {

    ZERO_STAR("ZERO_STAR"),
    HALF_STAR("HALF_STAR"),
    ONE_STAR("ONE_STAR"),
    ONE_AND_HALF_STAR("ONE_AND_HALF_STAR"),
    TWO_STARS("TWO_STARS"),
    TWO_AND_HALF_STARS("TWO_AND_HALF_STARS"),
    THREE_STARS("THREE_STARS"),
    THREE_AND_HALF_STARS("THREE_AND_HALF_STARS"),
    FOUR_STARS("FOUR_STARS"),
    FOUR_AND_HALF_STARS("FOUR_AND_HALF_STARS"),
    FIVE_STARS("FIVE_STARS");

    private String value;

    Rating(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String asString(Rating rating) {
        return rating.value;
    }

    public Double asDouble(Rating rating) {
        double val = 0.0;
        switch (rating) {
            case ZERO_STAR: val = 0.0; break;
            case HALF_STAR: val = 0.5; break;
            case ONE_STAR: val = 1.0; break;
            case ONE_AND_HALF_STAR: val = 1.5; break;
            case TWO_STARS: val = 2.0; break;
            case TWO_AND_HALF_STARS: val = 2.5; break;
            case THREE_STARS: val = 3.0; break;
            case THREE_AND_HALF_STARS: val = 3.5; break;
            case FOUR_STARS: val = 4.0; break;
            case FOUR_AND_HALF_STARS: val = 4.5; break;
            case FIVE_STARS: val = 5.0; break;
        }
        return val;
    }

    public Integer asInteger(Rating rating) {
        int val = 0;
        switch (rating) {
            case ZERO_STAR:
            case HALF_STAR:
                break;
            case ONE_STAR:
            case ONE_AND_HALF_STAR:
                val = 1; break;
            case TWO_STARS:
            case TWO_AND_HALF_STARS:
                val = 2; break;
            case THREE_STARS:
            case THREE_AND_HALF_STARS:
                val = 3; break;
            case FOUR_STARS:
            case FOUR_AND_HALF_STARS:
                val = 4; break;
            case FIVE_STARS: val = 5; break;
        }
        return val;
    }

    public static Rating fromString(String string) {
        return Rating.valueOf(string.toUpperCase());
    }

    public static Rating fromDouble(Double value) {
        if (value.equals(0.0)) {
            return Rating.ZERO_STAR;
        } else if (value.equals(0.5)) {
            return Rating.HALF_STAR;
        } else if (value.equals(1.0)) {
            return Rating.ONE_STAR;
        } else if (value.equals(1.5)) {
            return Rating.ONE_AND_HALF_STAR;
        } else if (value.equals(2.0)) {
            return Rating.TWO_STARS;
        } else if (value.equals(2.5)) {
            return Rating.TWO_AND_HALF_STARS;
        } else if (value.equals(3.0)) {
            return Rating.THREE_STARS;
        } else if (value.equals(3.5)) {
            return Rating.THREE_AND_HALF_STARS;
        } else if (value.equals(4.0)) {
            return Rating.FOUR_STARS;
        } else if (value.equals(4.5)) {
            return Rating.FOUR_AND_HALF_STARS;
        } else if (value.equals(5.0)) {
            return Rating.FIVE_STARS;
        }
        throw new IncorrectRatingException(value);
    }

    public static Rating fromInteger(Integer value) {
        if (value.equals(0)) {
            return Rating.ZERO_STAR;
        } else if (value.equals(1)) {
            return Rating.ONE_STAR;
        } else if (value.equals(2)) {
            return Rating.TWO_STARS;
        } else if (value.equals(3)) {
            return Rating.THREE_STARS;
        } else if (value.equals(4)) {
            return Rating.FOUR_STARS;
        } else if (value.equals(5)) {
            return Rating.FIVE_STARS;
        }
        throw new IncorrectRatingException(value);
    }

}