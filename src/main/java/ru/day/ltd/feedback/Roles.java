package ru.day.ltd.feedback;

public class Roles {
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_MODERATOR = "MODERATOR";
    public static final String ROLE_LANDLORD = "LANDLORD";
    public static final String ROLE_RENTER = "RENTER";

    enum Role {
        ADMIN,
        MODERATOR,
        LANDLORD,
        RENTER
    }

}
