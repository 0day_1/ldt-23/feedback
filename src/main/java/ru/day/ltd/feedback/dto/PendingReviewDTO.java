package ru.day.ltd.feedback.dto;

import java.util.Date;
import java.util.UUID;

public class PendingReviewDTO {
    UUID userId;
    UUID bookingEventId;
    Date pendingStartDate;
    Boolean isSatisfied;
    Date satisfiedAt;
    Boolean isDiscarded;
    Date discardedAt;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getBookingEventId() {
        return bookingEventId;
    }

    public void setBookingEventId(UUID bookingEventId) {
        this.bookingEventId = bookingEventId;
    }

    public Date getPendingStartDate() {
        return pendingStartDate;
    }

    public void setPendingStartDate(Date pendingStartDate) {
        this.pendingStartDate = pendingStartDate;
    }

    public Boolean getSatisfied() {
        return isSatisfied;
    }

    public void setSatisfied(Boolean satisfied) {
        isSatisfied = satisfied;
    }

    public Date getSatisfiedAt() {
        return satisfiedAt;
    }

    public void setSatisfiedAt(Date satisfiedAt) {
        this.satisfiedAt = satisfiedAt;
    }

    public Boolean getDiscarded() {
        return isDiscarded;
    }

    public void setDiscarded(Boolean discarded) {
        isDiscarded = discarded;
    }

    public Date getDiscardedAt() {
        return discardedAt;
    }

    public void setDiscardedAt(Date discardedAt) {
        this.discardedAt = discardedAt;
    }
}
