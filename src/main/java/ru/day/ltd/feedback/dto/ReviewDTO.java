package ru.day.ltd.feedback.dto;

import ru.day.ltd.feedback.entity.PendingReview;
import ru.day.ltd.feedback.entity.Rating;

import java.util.Date;
import java.util.UUID;

public class ReviewDTO {
    UUID id;
    PendingReview pendingReview;
    Date submitDate;
    Rating rating;
    String reviewText;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PendingReview getPendingReview() {
        return pendingReview;
    }

    public void setPendingReview(PendingReview pendingReview) {
        this.pendingReview = pendingReview;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }
}
