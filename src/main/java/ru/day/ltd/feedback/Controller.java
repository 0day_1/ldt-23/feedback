package ru.day.ltd.feedback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.day.ltd.feedback.dto.PendingReviewDTO;
import ru.day.ltd.feedback.dto.ReviewDTO;
import ru.day.ltd.feedback.entity.PendingReview;
import ru.day.ltd.feedback.entity.Rating;
import ru.day.ltd.feedback.entity.Review;
import ru.day.ltd.feedback.service.FindService;
import ru.day.ltd.feedback.service.PendingReviewService;
import ru.day.ltd.feedback.service.ReviewService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController()
@RequestMapping("/api/v1/feedback")
public class Controller {

    private final FindService findService;
    private final ReviewService reviewService;
    private final PendingReviewService pendingReviewService;

    @Autowired
    public Controller(FindService findService, ReviewService reviewService, PendingReviewService pendingReviewService) {
        this.findService = findService;
        this.reviewService = reviewService;
        this.pendingReviewService = pendingReviewService;
    }

    @PostMapping("/addPendingReview")
    public PendingReview addPendingReview(
        @RequestHeader("User-Id") String userId,
        @RequestBody PendingReviewDTO dto
    ) {
        return pendingReviewService.create(UUID.fromString(userId), dto.getBookingEventId());
    }

    @PostMapping("/addPendingReviews")
    public List<PendingReview> addPendingReview(
        @RequestHeader("User-Id") String userId,
        @RequestBody Iterable<PendingReviewDTO> dto
    ) {
        return StreamSupport
            .stream(dto.spliterator(), true)
            .map(i -> pendingReviewService.create(UUID.fromString(userId), i.getBookingEventId()))
            .collect(Collectors.toList());
    }

    @PostMapping("/addReview")
    public Review addReview(
        @RequestHeader("User-Id") String userId,
        @RequestBody ReviewDTO reviewDTO
    ) {
        return reviewService
                .addReview(
                        reviewDTO.getPendingReview().getId(),
                        reviewDTO.getReviewText(),
                        reviewDTO.getRating()
                );
    }

    @PostMapping("/discardPendingReview")
    public void discardPendingReview(
        @RequestHeader("User-Id") String userId,
        @RequestParam("pending-review-id") String pendingReviewId
    ) {
        pendingReviewService.discard(UUID.fromString(pendingReviewId));
    }

    @PostMapping("/deleteReview")
    public void deleteReview(
        @RequestHeader("User-Id") String userId,
        @RequestParam("review-id") String reviewId
    ) {
        reviewService.deleteReview(UUID.fromString(reviewId));
    }

    @PostMapping("/modifyReview")
    public Review modifyReview(
        @RequestHeader("User-Id") String userId,
        @RequestBody ReviewDTO reviewDTO
    ) {
        return reviewService
            .modifyReview(
                reviewDTO.getId(),
                reviewDTO.getReviewText(),
                reviewDTO.getRating()
            );
    }

    @PostMapping("/getRatingFromString")
    public Rating getRatingFromString(
        @RequestParam("rating") String rating
    ) {
        return Rating.fromString(rating);
    }

    @PostMapping("/getRatingFromDouble")
    public Rating getRatingFromDouble(
        @RequestParam("rating") Double rating
    ) {
        return Rating.fromDouble(rating);
    }

    @PostMapping("/getRatingFromInteger")
    public Rating getRatingFromInteger(
        @RequestParam("rating") Integer rating
    ) {
        return Rating.fromInteger(rating);
    }

}
