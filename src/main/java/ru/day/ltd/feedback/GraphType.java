package ru.day.ltd.feedback;

public final class GraphType {

    public static final String FETCH = "javax.persistence.fetchgraph";
    public static final String LOAD = "javax.persistence.loadgraph";

    private GraphType() {
    }
}
